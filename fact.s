    .section .data

format: .asciz "%d\n"

a: .quad 0x0
b: .quad 0x0
c: .quad 0x0
d: .quad 0x0
e: .quad 0x0
f: .quad 0x0
g: .quad 0x0
h: .quad 0x0
i: .quad 0x0
j: .quad 0x0
k: .quad 0x0
l: .quad 0x0
m: .quad 0x0
n: .quad 0x0
o: .quad 0x0
p: .quad 0x0
q: .quad 0x0
r: .quad 0x0
s: .quad 0x0
t: .quad 0x0
u: .quad 0x0
v: .quad 0x0
w: .quad 0x0
x: .quad 0x0
y: .quad 0x0
z: .quad 0x0

    .section .text
    .globl main
    .type main, @function
main:
	pushq	$0

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$1

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$2

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$3

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$4

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$5

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$6

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$7

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$8

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$9

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$10

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
	pushq	$11

	popq	%rdi
	call	factorial
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
main_end:
    call exit
