    .section .data

format: .asciz "%d\n"

a: .quad 0x0
b: .quad 0x0
c: .quad 0x0
d: .quad 0x0
e: .quad 0x0
f: .quad 0x0
g: .quad 0x0
h: .quad 0x0
i: .quad 0x0
j: .quad 0x0
k: .quad 0x0
l: .quad 0x0
m: .quad 0x0
n: .quad 0x0
o: .quad 0x0
p: .quad 0x0
q: .quad 0x0
r: .quad 0x0
s: .quad 0x0
t: .quad 0x0
u: .quad 0x0
v: .quad 0x0
w: .quad 0x0
x: .quad 0x0
y: .quad 0x0
z: .quad 0x0

    .section .text
    .globl main
    .type main, @function
main:
	pushq	$1000000
	popq	n(%rip)
	pushq	$100000000
	popq	s(%rip)
	pushq	$0
	popq	a(%rip)
L000:
	pushq	n(%rip)
	pushq	$0
	popq	%rbx
	popq	%rax
	cmp	%rbx,%rax
	movq	$0, %rcx
	setg	%cl
	xorb	$0, %cl
	jz	L001
	pushq	a(%rip)
	pushq	s(%rip)
	pushq	n(%rip)
	popq	%rbx
	popq	%rax
	cqto
	idivq	%rbx , %rax
	pushq	%rax
	popq	%rax
	popq	%rbx
	addq	%rbx , %rax
	pushq	%rax
	popq	a(%rip)
	pushq	n(%rip)
	pushq	$1
	popq	%rbx
	popq	%rax
	subq	%rbx , %rax
	pushq	%rax
	popq	n(%rip)
	jmp	L000
L001:
	pushq	a(%rip)
	pushq	s(%rip)
	pushq	$1000
	popq	%rbx
	popq	%rax
	cqto
	idivq	%rbx , %rax
	pushq	%rax
	popq	%rbx
	popq	%rax
	cqto
	idivq	%rbx , %rax
	pushq	%rax

	popq	%rax
	pushq	%rdi
	pushq	%rsi
	movq	$format, %rdi
	movq	%rax, %rsi
	xor	%rax, %rax
	call	printf
	popq	%rsi
	popq	%rdi
main_end:
    call exit
